var map = {
    el: document.getElementById("viewport"),
    running: true,
    getMapX: function () {
        var style = window.getComputedStyle(this.el),
            pos = style['background-position-x'],
            bgPosX = parseInt(pos.substr(0, pos.length - 2), 10);

        return bgPosX;
    },
    moveRight: function () {
        var bgPosX = this.getMapX(),
            bgnewPos = bgPosX - 4;

        if(bgPosX < (-7300)){
            return
        }

        if(!this.running){
            this.el.classList.add("bg-move-left");
            this.el.style.webkitAnimation = "";
            this.running = true;
        }

        this.el.style['background-position-x'] = bgnewPos + "px";
    },
    moveLeft: function () {
        var bgPosX = this.getMapX(),
            bgnewPos = bgPosX + 4;

        if(bgPosX > 0){
            return;
        }

        if(!this.running){
            this.el.classList.add("bg-move-right");            
            this.running = true;
        }

        this.el.style['background-position-x'] = bgnewPos + "px";
    },
    stop: function() {
        if(this.running) {
            this.el.classList.remove("bg-move-left");
            this.el.classList.remove("bg-move-right");
            this.running = false;
        }
    }
};

var zero = {
    el: document.getElementById("zero"),
    transformed: false,
    running: false,
    jump: false,
    falling: false,
    getPosX: function () {
        var style = window.getComputedStyle(this.el),
            left  = style.left,
            posX  = parseInt(left.substr(0, left.length - 2), 10);        

        return posX;
    },
    getPosY: function () {
        var style = window.getComputedStyle(this.el),
            top  = style.top,
            posY  = parseInt(top.substr(0, top.length - 2), 10);        

        return posY;
    },
    currentPos: function () {
        return {x: map.getMapX(), y:this.getPosY()};
    },
    dojump: function(){
        var posY   = this.getPosY(),
            newPos = posY - 20;
        
        if (posY < 0) {        
            return;
        }
        
        /*if (this.falling) {
            this.el.style.top = newPos + "px";
            console.log(this.el.style.top);
            this.jump = true;
            this.fall();
        }else */
        if (this.falling) {
            this.jump = false;
            this.fall();
        }
        else if (!this.jump) {
            this.el.classList.remove("zero-fall");
            this.el.classList.remove("zero-stand");
            this.el.classList.add("zero-smalljump");
            this.el.style.webkitAnimation = "";
            this.jump = true;
            //this.el.style.top = newPos + "px";

            this.el.addEventListener("webkitAnimationEnd", function(){ 
                zero.el.classList.add("zero-fall"); 
                zero.el.classList.remove("zero-stand");
                zero.falling = false;
            }, false);
        }        
    },
    fall: function() {
        var posY   = this.getPosY(),
            newPos = posY + 80;
        
        if (posY > 150) {            
            return;
        }
        
        if (this.jump) {
            this.el.classList.remove("zero-jump");
            this.el.classList.add("zero-fall");
            this.el.style.webkitAnimation = "";
            this.jump = false;
            this.falling = true;
            this.el.style.top = "150px";
        }
        this.el.addEventListener("webkitAnimationEnd", function(){ 
            zero.el.classList.remove("zero-fall"); 
            zero.el.classList.add("zero-stand");
            zero.falling = false;
        }, false); 
    },    
    standStill: function () {
        code="";
        if (this.running) {
            this.el.classList.remove("zero-run-right");
            this.el.classList.remove("zero-run-left");
            //this.el.classList.add("zero-stand");
            this.running = false;            
        }
        if (this.jump){
            this.el.classList.remove("zero-smalljump");
            this.el.classList.add("zero-fall");
            this.jump = false;

            this.el.addEventListener("webkitAnimationEnd", function(){ 
                zero.el.classList.remove("zero-fall"); 
                zero.el.classList.add("zero-stand");
                zero.falling = false;
            }, false);
        }
    },
    moveRight: function () {
        var posX   = this.getPosX(),
            newPos = posX + 3;
        
        if (posX > 600) {            
            return;
        }
        
        if (!this.running) {
            this.el.classList.remove("zero-stand");
            this.el.classList.add("zero-run-right");
            this.el.style.webkitAnimation = "";
            this.running = true;
        }
        
        if (this.transformed) {
            this.el.style.webkitTransform = "";
            this.transformed = !this.transformed;
        }
        
        //this.el.style.left = newPos + "px";        
    },
    moveLeft: function () {
        var posX   = this.getPosX(),
            newPos = posX - 3;
        
        if (posX < 0) {
            return;
        }
        
        if (!this.running) {
            this.el.classList.remove("zero-stand");
            this.el.classList.add("zero-run-left");
            this.running = true;
        }
        
        if (!this.transformed) {
            this.el.style.webkitTransform = "rotateY(180deg)";
            this.transformed = !this.transformed;
        }
        
        //this.el.style.left = newPos + "px";
    }
};

var key = {
    37:false,
    38:false,
    39:false
};

var ProsesKeyUp = function () {
    zero.standStill();     
        zero.fall();
    map.stop();    
    websocket.send("stand");
};

var code = "";
window.addEventListener("keydown", function (e) {
    code = e.keyCode;
});

window.addEventListener("keyup", function () {
    zero.standStill();
});

function loopGame (){

    switch (code) {
    case 37:
        map.moveLeft();
        zero.moveLeft();
        console.log(zero.currentPos());
        break;
    case 38:        
        zero.dojump();
        console.log(zero.currentPos());
        break;    
    case 39:
        map.moveRight();
        zero.moveRight();
        console.log(zero.currentPos());
        break;
    }
    
    setTimeout(loopGame, 20);
}
loopGame();
